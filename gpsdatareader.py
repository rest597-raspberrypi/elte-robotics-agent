import pynmea2
from math import radians, cos, sin, asin, sqrt


# Source: https://stackoverflow.com/questions/4913349/haversine-formula-in-python-bearing-and-distance-between-two-gps-points
@staticmethod
def haversine_distance(gps1, gps2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [gps1.longitude, gps1.latitude, gps2.longitude, gps2.latitude])
    # haversine formula 
    d_lon = lon2 - lon1
    d_lat = lat2 - lat1
    a = sin(d_lat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(d_lon / 2) ** 2
    c = 2 * asin(sqrt(a))
    km = 6367 * c
    return km

@staticmethod
def read_gps_data(f_name):
    if f_name == '':
        return None
    content = []
    with open(f_name) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    return content

@staticmethod
def get_gps_data_objects(gps_lines):
    return map(lambda x: pynmea2.parse(x), gps_lines)

@staticmethod
def get_gps_data_object(gps_line):
    return pynmea2.parse(gps_line)

@staticmethod
def get_gps_data(f_name):
    return get_gps_data_objects(read_gps_data(f_name))

@staticmethod
def get_minimum_distance_point(gps_objs, robot_location):
    min_index = 0
    min = haversine_distance(gps_objs[min_index], robot_location)
    for ind in range(1, len(gps_objs) - 1):
        distance = haversine_distance(gps_objs[ind], robot_location)
        if min > distance:
            min_index = ind
            min = distance
    return (min_index, min)