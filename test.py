
import config
import gpsdatareader
import compasshelper


robot_location = gpsdatareader.get_gps_data_object('$GPGGA,164600.000,4728.4547,N,01903.4810,E,1,4,4.23,96.1,M,41.1,M,,*6F')
gps_objs = gpsdatareader.get_gps_data(config.GPSDataFile)

min_index, min = gpsdatareader.get_minimum_distance_point(gps_objs, robot_location)

bearing = compasshelper.calculate_initial_compass_bearing(gps_objs[min_index], robot_location)
print(min_index)
print("Hello world!")