from motor_c import Motor, MotorStatus, MotorSpeed, TurnType, MotorTurn
from calculator import Calculator
import time


class Controller:
    def __init__(self):
        self.motor = Motor
        self.calculator = Calculator

    def start(self, duration_between_steps_with_second=5):
        while self.motor.status != MotorStatus.FINISHED:
            direction, c_time = self.calculator.calculate(self.motor)
            if direction == TurnType.FORWARD:
                self.motor.forward(MotorSpeed.FORWARD_MAX)
            elif direction == TurnType.BACKWARD:
                self.motor.forward(MotorSpeed.BACKWARD_MAX)
            elif direction == TurnType.LEFT:
                self.motor.turn(MotorTurn.LEFT_MAX, c_time)
            elif direction == TurnType.RIGHT:
                self.motor.turn(MotorTurn.RIGHT_MAX, c_time)
            else:
                self.motor.stop_motor
            time.sleep(duration_between_steps_with_second)
