import serial


class SerialReader:
    def __init__(self):
        self.c_serial = serial.Serial(
            port='/dev/ttyAMA0',
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1
        )

    def read(self):
        result = None
        while result is None:
            read_line = self.c_serial.readline()
            if "GPGGA" in read_line:
                result = read_line
        return result
