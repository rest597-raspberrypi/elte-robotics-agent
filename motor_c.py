import time
import RPi.GPIO as GPIO


class Motor:

    status = MotorStatus.STANDING

    STAND_SPEED = 75

    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(21, GPIO.OUT)  # 20-21 outputs
        GPIO.setup(20, GPIO.OUT)

        self.drive = GPIO.PWM(21, 50)
        self.drive.start(self.STAND_SPEED / 10)

        self.turn = GPIO.PWM(20, 50)
        self.turn.start(self.STAND_SPEED / 10)

    def forward(self, speed=70):
        self.drive.ChangeDutyCycle(self.STAND_SPEED / 10)
        self.drive.ChangeDutyCycle(speed / 10.0)

        self.status = MotorStatus.MOVING

    def turn(self, speed=70, duration=0):
        self.drive.ChangeDutyCycle(self.STAND_SPEED / 10)
        self.turn.ChangeDutyCycle(self.STAND_SPEED / 10)
        self.turn.ChangeDutyCycle(speed / 10.0)
        time.sleep(duration)
        self.turn.ChangeDutyCycle(self.STAND_SPEED / 10)

        self.status = MotorStatus.MOVING

    def stop_motor(self):
        self.drive.stop()
        self.turn.stop()
        GPIO.cleanup()

        self.status = MotorStatus.STANDING


class MotorStatus:
    STANDING = "standing"
    MOVING = "moving"
    FINISHED = "finished"

class MotorSpeed:
    FORWARD_MAX = 40
    BACKWARD_MAX = 100
    #FORWARD_SLOWLY = 55 Go with just max speed
    #BACKWARD_SLOWLY = 85 Go with just max speed
    STAND = 70

class MotorTurn:
    RIGHT_MAX = 40
    LEFT_MAX = 100
    #RIGHT_SLOWLY = 55 Turn with just max speed, because it can't turn slowly well
    #LEFT_SLOWLY = 85 Turn with just max speed, because it can't turn slowly well
    STAND = 70

class TurnType:
    FORWARD = 0
    BACKWARD = 1
    RIGHT = 2
    LEFT = 3
    NOTHING = 4