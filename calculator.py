from motor_c import MotorStatus, TurnType
from serial_read import SerialReader
import gpsdatareader
import config
import compasshelper


class Calculator:
    def __init__(self):
        self.serial_reader = SerialReader
        self.reachable_gps_coordinations = gpsdatareader.get_gps_data(config.GPSDataFile)

    def calculate(self, motor):
        input = self.serial_reader.read
        robot_location = gpsdatareader.get_gps_data_object(input)
        near_gps_coordination = gpsdatareader.get_minimum_distance_point(self.reachable_gps_coordinations,
                                                                         robot_location)
        return self.decide_distance(robot_location, near_gps_coordination, motor)

    def decide_distance(self, robot_location, near_gps_coordination, motor):
        # TODO check just 2 value in robot_location and in near_gps_coordination
        if robot_location == near_gps_coordination:
            if len(self.reachable_gps_coordinations) <= 0:
                motor.status = MotorStatus.FINISHED
                return TurnType.NOTHING, 0  # type of turn, time with second
            else:
                # TODO do not compare the object, we should compare just 2 value instead (remove with condition!)
                self.reachable_gps_coordinations.remove(
                    near_gps_coordination)
                near_gps_coordination = gpsdatareader.get_minimum_distance_point(self.reachable_gps_coordinations,
                                                                                 robot_location)
                self.decide_distance(robot_location, near_gps_coordination, motor)
        else:
            degree = compasshelper.calculate_initial_compass_bearing(robot_location, near_gps_coordination)
            if degree == 0.0:
                return TurnType.FORWARD, 0 # type of turn, time with second
            elif degree < 180:
                return TurnType.RIGHT, self.calculate_time_with_degree(degree) # type of turn, time with second
            else:
                return TurnType.LEFT, self.calculate_time_with_degree(degree) # type of turn, time with second

    @staticmethod
    def calculate_time_with_degree(degree):
        return degree / 45  # circa 1 second equals 45 degree
